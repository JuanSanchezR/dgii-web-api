IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Users')
BEGIN
    CREATE TABLE Users (
        UserID int PRIMARY KEY IDENTITY,
        UserName nvarchar(50),
        RncNumberID nvarchar(50),
        Type nvarchar(50),
        Status bit
    );
END

INSERT INTO Users (UserName, RncNumberID, Type, Status)
VALUES ('JUAN PEREZ', '98754321012','PERSONA FISICA', 1),
       ('FARMACIA TU SALUD', '123456789','PERSONA JURIDICA', 0);


IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Transactions')
BEGIN
    CREATE TABLE Transactions (
        TransactionID int PRIMARY KEY IDENTITY,
        NCF nvarchar(13),
        UserID int FOREIGN KEY REFERENCES Users(UserID),
        RncNumberID nvarchar(50),
        Amount decimal(10, 2),
        Tax decimal(10, 2),
        TransactionDate datetime
    );  
END


INSERT INTO Transactions (NCF, UserID, RncNumberID, Amount, Tax, TransactionDate)
VALUES ("E310000000001", 1, '98754321012', 200.03, 36.01, GETDATE()),
       ("E310000000002", 1, '123456789', 1000.30, 180.10, GETDATE());

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Logs')
BEGIN
    CREATE TABLE Logs (
        LogID int PRIMARY KEY IDENTITY,
        LogDate datetime,
        LogDescription nvarchar(255)
    );
END


INSERT INTO Logs (LogDate, LogDescription)
VALUES (GETDATE(), 'Estado de transacción: exitosa'),
       (GETDATE(), 'Estado de transacción: exitosa');