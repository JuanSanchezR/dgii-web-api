﻿using WebApplication1.Models;
using WebApplication1.Request;
using WebApplication1.Utils;

namespace WebApplication1.Validations
{
    public class TransactionValidations
    {
        public static Boolean esNumerico(String valor)
        {
            Boolean esNumerico = true;
            Int64 n;
            esNumerico = Int64.TryParse(valor, out n);
            return esNumerico;
        }

        public static string ValidTransaction(TransactionRequest transaction)
        {
            String resp = "ok";

            if (transaction == null)
            {
                resp = "Invalid data";
                return resp;
            }

            if (transaction.usuarioid == null || transaction.usuarioid == "")
            {
                resp = "Usuario invalido";
                return resp;
            }

            if (transaction.monto == null || transaction.monto == "")
            {
                resp = "monto invalido";
                return resp;
            }

            if (!esNumerico(transaction.usuarioid))
            {
                resp = "Usuario invalido";
                return resp;
            }

            if (!esNumerico(transaction.monto))
            {
                resp = "monto invalido";
                return resp;
            }

            if (!EntitiesToDB.validUserExist(int.Parse(transaction.usuarioid)))
            {
                resp = "this user does not exist";
                return resp;
            }

            return resp;
        }

        public static string ValidTransactionByUserId(string userId)
        {
            String resp = "ok";

            if (userId == null)
            {
                resp = "data not found";
                return resp;
            }

            if (!esNumerico(userId))
            {
                resp = "data not found";
                return resp;
            }

            if (!EntitiesToDB.validUserExist(int.Parse(userId)))
            {
                resp = "this user does not exist";
                return resp;
            }

            return resp;
        }

        public static string ValidTransactionByNCF(string ncf)
        {
            String resp = "ok";

            if (ncf == null)
            {
                resp = "data not found";
                return resp;
            }

            if (!EntitiesToDB.validNCFExist(ncf))
            {
                resp = "this ncf does not exist";
                return resp;
            }

            return resp;
        }
    }
}
