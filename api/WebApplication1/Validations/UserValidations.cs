﻿using WebApplication1.Models;
using WebApplication1.Request;
using WebApplication1.Utils;

namespace WebApplication1.Validations
{
    public class UserValidations
    {
        public static Boolean esNumerico(String valor)
        {
            Boolean esNumerico = true;
            Int64 n;
            esNumerico = Int64.TryParse(valor, out n);
            return esNumerico;
        }

        public static string Validuser(UserRequest user)
        {
            String resp = "ok";

            if (user == null)
            {
                resp = "Invalid data";
                return resp;
            }

            if (!esNumerico(user.rncCedula))
            {
                resp = "rncCedula invalido";
                return resp;
            }

            if (user.nombre == null || user.nombre == "")
            {
                resp = "Nombre invalido";
                return resp;
            }

            if (user.rncCedula == null || user.rncCedula == "")
            {
                resp = "rncCedula invalido";
                return resp;
            }

            if (user.tipo == null || user.tipo == "")
            {
                resp = "tipo invalido";
                return resp;
            }

            if (EntitiesToDB.validUserNumberidExist(user.rncCedula))
            {
                resp = "rncCedula ya existe";
                return resp;
            }

            return resp;
        }

        public static string ValidEditUser(UserRequest user)
        {
            String resp = "ok";

            if (user == null)
            {
                resp = "Invalid data";
                return resp;
            }

            if (!esNumerico(user.rncCedula))
            {
                resp = "rncCedula invalido";
                return resp;
            }

            if (user.nombre == null || user.nombre == "")
            {
                resp = "Nombre invalido";
                return resp;
            }

            if (user.rncCedula == null || user.rncCedula == "")
            {
                resp = "rncCedula invalido";
                return resp;
            }

            if (user.tipo == null || user.tipo == "")
            {
                resp = "tipo invalido";
                return resp;
            }

            if (!EntitiesToDB.validUserExist(user.id))
            {
                resp = "usuario no existe";
                return resp;
            }

            if (EntitiesToDB.validEditRncNumberidExist(user.rncCedula, user.id))
            {
                resp = "rncCedula ya existe";
                return resp;
            }

            return resp;
        }

        public static string ValidDeleteuUser(int user)
        {
            String resp = "ok";

            if (user.ToString() == null)
            {
                resp = "Invalid data";
                return resp;
            }

            if (!EntitiesToDB.validUserStatus(user))
            {
                resp = "inactive user";
                return resp;
            }

            if (!EntitiesToDB.validUserExist(user))
            {
                resp = "this user does not exist";
                return resp;
            }

            return resp;
        }
    }
}