﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using WebApplication1.Models;
using WebApplication1.Request;

namespace WebApplication1.Utils
{
    public static class EntitiesToDB
    {
        #region Log
        public static bool insertLog(string descripcion)
        {
            bool blnResp = false;
            var ctx = new DevContext();

            try
            {
                Logs log = new Logs();
                log.LogDate = DateTime.Now;
                log.LogDescription = descripcion;

                ctx.Logs.Add(log);
                ctx.SaveChanges();

                blnResp = true;
            }
            catch (Exception)
            {
                throw;
            }
            return blnResp;
        }
        #endregion

        #region User
        public static bool insertUser(UserRequest user)
        {
            bool blnResp = false;
            var ctx = new DevContext();

            try
            {
                Users usr = new Users();
                usr.UserName = user.nombre;
                usr.RncNumberID = user.rncCedula;
                usr.Type = user.tipo;
                usr.Status = true;

                ctx.Users.Add(usr);
                ctx.SaveChanges();

                blnResp = true;
            }
            catch (Exception)
            {
                throw;
            }
            return blnResp;
        }

        public static List<UserRequest> GetUsers(string userId)
        {
            using (var ctx = new DevContext())
            {
                try
                {
                    if (userId == "*")
                    {
                        var users = from a in ctx.Users
                                    select new UserRequest
                                    {
                                        id = a.UserID,
                                        nombre = a.UserName,
                                        rncCedula = a.RncNumberID,
                                        tipo = a.Type,
                                        estatus = (a.Status == true) ? "Activo" : "Inactivo",
                                    };
                        return users.ToList();
                    }
                    else
                    {
                        var user = from a in ctx.Users
                                   where a.UserID == int.Parse(userId)
                                   select new UserRequest
                                   {
                                       id = a.UserID,
                                       nombre = a.UserName,
                                       rncCedula = a.RncNumberID,
                                       tipo = a.Type,
                                       estatus = (a.Status == true) ? "Activo" : "Inactivo",
                                   };
                        return user.ToList();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static Boolean UpdateUser(UserRequest user)
        {
            bool blnResp = false;
            int process_id = user.id;
            var ctx = new DevContext();

            var upd = (from a in ctx.Users
                       where a.UserID == process_id
                       select a).FirstOrDefault();

            if (upd != null)
            {
                try
                {
                    upd.UserName = user.nombre;
                    upd.RncNumberID = user.rncCedula;
                    upd.Type = user.tipo;
                    ctx.SaveChanges();
                    blnResp = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return blnResp;
        }

        public static Boolean DeleteUser(int user)
        {
            bool blnResp = false;
            int process_id = user;
            var ctx = new DevContext();

            var upd = (from a in ctx.Users
                       where a.UserID == process_id
                       select a).FirstOrDefault();

            if (upd != null)
            {
                try
                {
                    bool estatus = false;
                    upd.Status = estatus;
                    ctx.SaveChanges();
                    blnResp = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return blnResp;
        }
        #endregion

        #region Transaction
        public static bool insertTransaction(TransactionRequest transaction)
        {
            bool blnResp = false;
            var ctx = new DevContext();

            try
            {
                Transactions trs = new Transactions();
                trs.NCF = transaction.ncf;
                trs.UserID = int.Parse(transaction.usuarioid);
                trs.RncNumberID = transaction.rncCedula;
                trs.Amount = decimal.Parse(transaction.monto);
                trs.Tax = decimal.Parse(transaction.itbis18);
                trs.TransactionDate = DateTime.Now;

                ctx.Transactions.Add(trs);
                ctx.SaveChanges();

                blnResp = true;
            }
            catch (Exception)
            {
                throw;
            }
            return blnResp;
        }

        public static List<TransactionRequest> GetTransactionByUserId(int process_id)
        {
            using (var ctx = new DevContext())
            {
                try
                {
                    var dtTransa = from a in ctx.Transactions
                                   where a.UserID == process_id
                                   select new TransactionRequest
                                   {
                                       transactionid = a.TransactionID,
                                       ncf = a.NCF,
                                       usuarioid = a.UserID.ToString(),
                                       rncCedula = a.RncNumberID,
                                       monto = a.Amount.ToString(),
                                       itbis18 = a.Tax.ToString(),
                                       fecha = a.TransactionDate,
                                   };

                    return dtTransa.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static List<TransactionRequest> GetTransactionByNCF(string ncf)
        {
            using (var ctx = new DevContext())
            {
                try
                {
                    var dtTransa = from a in ctx.Transactions
                                   where a.NCF == ncf
                                   select new TransactionRequest
                                   {
                                       transactionid = a.TransactionID,
                                       ncf = a.NCF,
                                       usuarioid = a.UserID.ToString(),
                                       rncCedula = a.RncNumberID,
                                       monto = a.Amount.ToString(),
                                       itbis18 = a.Tax.ToString(),
                                       fecha = a.TransactionDate,
                                   };

                    return dtTransa.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion

        #region Valid
        public static bool validUserStatus(int id)
        {
            var ctx = new DevContext();
            bool Resp = false;

            var cStatus = (from a in ctx.Users
                           where a.UserID == id && a.Status == true
                           select a.UserName).Count();

            if (cStatus > 0)
            {
                Resp = true;
            }

            return Resp;
        }

        public static bool validUserNumberidExist(string id)
        {
            var ctx = new DevContext();
            bool Resp = false;

            var exist = (from a in ctx.Users
                         where a.RncNumberID == id
                         select a.UserID).Count();

            if (exist > 0)
            {
                Resp = true;
            }

            return Resp;
        }

        public static bool validUserExist(int id)
        {
            var ctx = new DevContext();
            bool Resp = false;

            var exist = (from a in ctx.Users
                         where a.UserID == id
                         select a.UserID).Count();

            if (exist > 0)
            {
                Resp = true;
            }

            return Resp;
        }

        public static bool validNCFExist(string ncf)
        {
            var ctx = new DevContext();
            bool Resp = false;

            var exist = (from a in ctx.Transactions
                         where a.NCF == ncf
                         select a.NCF).Count();

            if (exist > 0)
            {
                Resp = true;
            }

            return Resp;
        }

        public static bool validEditRncNumberidExist(string numberid, int user)
        {
            var ctx = new DevContext();
            bool Resp = false;

            var exist = (from a in ctx.Users
                         where a.RncNumberID == numberid && a.RncNumberID == user.ToString()
                         select a.RncNumberID).Count();

            if (exist > 0)
            {
                Resp = true;
            }

            return Resp;
        }

        public static List<String> GetNFC()
        {
            using (var ctx = new DevContext())
            {
                try
                {
                    var nfc = (from a in ctx.Transactions
                                 select a.NCF);

                    return nfc.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static string GetNumberId(string userID)
        {
            using (var ctx = new DevContext())
            {
                try
                {
                    var nfc = (from a in ctx.Users
                               where a.UserID == int.Parse(userID)
                               select a.RncNumberID).FirstOrDefault();

                    return nfc;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion
    }
}
