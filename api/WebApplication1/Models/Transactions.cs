﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public partial class Transactions
    {
        public int TransactionID { get; set; }
        public string NCF { get; set; }
        public int UserID { get; set; }
        public string RncNumberID { get; set; }
        public decimal Amount { get; set; }
        public decimal Tax { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
