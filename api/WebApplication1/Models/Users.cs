﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public partial class Users
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string RncNumberID { get; set; }
        public string Type { get; set; }
        public bool Status { get; set; }
    }
}