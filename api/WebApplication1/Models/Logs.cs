﻿using System;
using System.Collections.Generic;


namespace WebApplication1.Models
{
    public partial class Logs
    {
        public int LogID { get; set; }
        public DateTime LogDate { get; set; }
        public string  LogDescription { get; set; }
    }
}
