﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace WebApplication1.Models
{
    public partial class DevContext : DbContext
    {
        public DevContext()
        {
        }

        public DevContext(DbContextOptions<DevContext> options)
        : base(options)
        {
        }

        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
                var connectionString = configuration.GetConnectionString("DB");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("Users");

                entity.HasKey(e => e.UserID);

                entity.Property(e => e.UserName).HasMaxLength(50);

                entity.Property(e => e.RncNumberID).HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Status);

            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.ToTable("Transactions");

                entity.HasKey(e => e.TransactionID);

                entity.Property(e => e.NCF).HasMaxLength(13);

                entity.Property(e => e.UserID);

                entity.Property(e => e.RncNumberID).HasMaxLength(50);

                entity.Property(e => e.Amount);

                entity.Property(e => e.Tax);

                entity.Property(e => e.TransactionDate);

            });

            modelBuilder.Entity<Logs>(entity =>
            {
                entity.ToTable("Logs");

                entity.HasKey(e => e.LogID);

                entity.Property(e => e.LogDate);

                entity.Property(e => e.LogDescription).HasMaxLength(255);

            });

            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
