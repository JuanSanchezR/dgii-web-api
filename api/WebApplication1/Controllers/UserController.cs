﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Configuration;
using System.Net;
using WebApplication1.Models;
using WebApplication1.Request;
using WebApplication1.Utils;
using WebApplication1.Validations;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : Controller
    {
        #region Agregar nuevos usuarios
        [HttpPost]
        public IActionResult Create([FromBody] UserRequest model)
        {
            string validation = UserValidations.Validuser(model);

            if (validation != "ok")
            {
                var resp = new
                {
                    message = "The request is invalid.",
                    error = validation
                };

                return BadRequest(resp);
            }
            else
            {
                if (!EntitiesToDB.insertUser(model))
                {
                    EntitiesToDB.insertLog("Nuevo usuario: error de ingreso usuario");
                    string[] arrError;
                    arrError = new string[1];

                    var resp = new
                    {
                        message = "Fail insert user.",
                        error = arrError
                    };

                    return StatusCode(StatusCodes.Status500InternalServerError,resp);
                }
                else
                {
                    EntitiesToDB.insertLog("Nuevo usuario: ingreso correcto");
                    var resp = new
                    {
                        message = "ok",
                    };
                    return StatusCode(StatusCodes.Status201Created,resp);

                }
            }
        }
        #endregion

        #region Consultar usuario
        [HttpGet("{userId:int}")]
        public IActionResult consult(string userId)
        {
            var users = EntitiesToDB.GetUsers(userId);

            if (users.ToList().Count == 0)
            {
                EntitiesToDB.insertLog("Consulta de usuarios: no se econtraron usuarios");

                var resp = new
                {
                    message = "The user was not found.",
                };

                return StatusCode(StatusCodes.Status404NotFound, resp);
            }
            else
            {
                return Ok(users.ToList());
            }
        }
        #endregion

        #region Consultar usuarios
        [HttpGet]
        public IActionResult consult()
        {
            var users = EntitiesToDB.GetUsers("*");

            if (users.ToList().Count == 0)
            {
                EntitiesToDB.insertLog("Consulta de usuarios: no se econtraron usuarios");

                var resp = new
                {
                    message = "The user was not found.",
                };

                return StatusCode(StatusCodes.Status404NotFound, resp);
            }
            else
            {
                return Ok(users.ToList());
            }
        }
        #endregion

        #region Editar usuarios
        [HttpPut]
        public IActionResult Edit([FromBody] UserRequest user)
        {
            if (user == null)
                return BadRequest(HttpStatusCode.BadRequest);

            string validation = UserValidations.ValidEditUser(user);
            if (validation != "ok")
            {
                EntitiesToDB.insertLog("Editor usuario: no se enonctró usuario: " + user.id );
                string[] arrError;
                arrError = new string[1] { validation };
                var resp = new
                {
                    code = "99",
                    message = "The request is invalid."
                };

                return BadRequest(resp);
            }

            //edit               
            if (!EntitiesToDB.UpdateUser(user))
            {
                EntitiesToDB.insertLog("Editor usuario: error actualizacion usuario #" + user.id);

                var resp = new
                {
                    code = "99",
                    message = "The request is invalid."
                };

                return BadRequest(resp);
            }
            else
            {
                EntitiesToDB.insertLog("Editor usuario: actualización corecta usuario #" + user.id);
                var resp = new
                {
                    code = "00",
                    message = "ok"
                };

                return Ok(resp);
            }
        }
        #endregion

        #region Eliminar usuarios
        [HttpDelete]
        [Route("{userId}")]
        public IActionResult Delete(int userId)
        {
            string validation = UserValidations.ValidDeleteuUser(userId);
            if (validation != "ok")
            {
                EntitiesToDB.insertLog("Eliminación usuario: error validacion");
                string[] arrError;
                arrError = new string[1] { validation };
                var resp = new
                {
                    code = "99",
                    message = "The request is invalid.",
                    error = arrError
                };

                return BadRequest(resp);
            }

            string msg = "";
            //atualizar usuario                
            if (!EntitiesToDB.DeleteUser(userId))
            {
                EntitiesToDB.insertLog("Eliminación usuario: error al eliminar usuario");
                string[] arrError;
                arrError = new string[1] { msg };

                var resp = new
                {
                    code = "99",
                    message = "The request is invalid.",
                    error = arrError
                };

                return BadRequest(resp);
            }
            else
            {
                EntitiesToDB.insertLog("Eliminación usuario: eliminación correcta");
                var resp = new
                {
                    code = "00",
                    message = "ok"
                };

                return Ok(resp);

            }
        }
        #endregion
    }
}