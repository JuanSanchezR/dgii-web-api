﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Net;
using WebApplication1.Models;
using WebApplication1.Request;
using WebApplication1.Utils;
using WebApplication1.Validations;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionController : Controller
    {
        #region Agregar nueva transferencia
        [HttpPost]
        public IActionResult Create([FromBody] TransactionRequest model)
        {
            string resultUser = "";
            resultUser = TransactionValidations.ValidTransaction(model);

            if (resultUser != "ok")
            {
                var resp = new
                {
                    message = "The request is invalid.",
                    error = resultUser
                };

                return BadRequest(resp);
            }
            else
            {
                //secuencia automatica de 10 digitos
                var nfc = EntitiesToDB.GetNFC().LastOrDefault();
                string cadena = nfc.Length >= 10 ? nfc.Substring(nfc.Length - 10) : nfc;
                int secuencial = int.Parse(cadena);
                secuencial++;
                cadena = secuencial.ToString("D10");
                model.ncf = "E31"+ cadena;

                model.itbis18 = (decimal.Parse(model.monto) * 18 / 100).ToString();

                var Cedula = EntitiesToDB.GetNumberId(model.usuarioid);
                model.rncCedula = Cedula.ToString();

                if (!EntitiesToDB.insertTransaction(model))
                {
                    EntitiesToDB.insertLog("Nueva transacion: error de ingreso transacion");
                    string[] arrError;
                    arrError = new string[1];

                    var resp = new
                    {
                        message = "The request is invalid.",
                        error = arrError
                    };

                    return StatusCode(StatusCodes.Status500InternalServerError, resp);
                }
                else
                {
                    EntitiesToDB.insertLog("Nueva transacion: ingreso correcto");
                    var resp = new
                    {
                        message = "ok",
                    };

                    return StatusCode(StatusCodes.Status201Created, resp);
                }
            }
        }
        #endregion

        #region Consultar transaciones por userid
        [HttpGet("{userId:int}")]
        public IActionResult GetByUserId(string userId)
        {
            string resultValid = "";
            resultValid = TransactionValidations.ValidTransactionByUserId(userId);
            if (resultValid != "ok")
            {
                EntitiesToDB.insertLog("Consulta de transacciones: validacion consulta incorrecta");
                string[] arrError;
                arrError = new string[1] { resultValid };

                var resp = new
                {
                    message = "The request is invalid.",
                    error = arrError
                };

                return BadRequest(resp);
            }

            int process_id = int.Parse(userId);
            var dataTransactions = EntitiesToDB.GetTransactionByUserId(process_id);

            if (dataTransactions.ToList().Count == 0)
            {
                EntitiesToDB.insertLog("Consulta de transacciones: no se encontró transacciones para el usuario: " + process_id + "");
                string[] arrError;
                arrError = new string[1] { "data not found" };

                var resp = new
                {
                    message = "The request is invalid.",
                    error = arrError
                };

                return StatusCode(StatusCodes.Status404NotFound, resp);
            }
            else
            {
                return Ok(dataTransactions.ToList());
            }
        }
        #endregion

        #region Consultar transaciones por ncf
        [HttpGet("{ncf}")]
        public IActionResult GetByNCF(string ncf)
        {
            string resultValid = "";
            resultValid = TransactionValidations.ValidTransactionByNCF(ncf);
            if (resultValid != "ok")
            {
                EntitiesToDB.insertLog("Consulta de transacciones: validacion consulta incorrecta");
                string[] arrError;
                arrError = new string[1] { resultValid };

                var resp = new
                {
                    message = "The request is invalid.",
                    error = arrError
                };

                return BadRequest(resp);
            }

            var dataTransactions = EntitiesToDB.GetTransactionByNCF(ncf);

            if (dataTransactions.ToList().Count == 0)
            {
                EntitiesToDB.insertLog("Consulta de transacciones: no se encontró la transaccion para el ncf: " + ncf + "");
                string[] arrError;
                arrError = new string[1] { "data not found" };

                var resp = new
                {
                    message = "The request is invalid.",
                    error = arrError
                };

                return StatusCode(StatusCodes.Status404NotFound, resp);
            }
            else
            {
                return Ok(dataTransactions.ToList());
            }
        }
        #endregion
    }
}
