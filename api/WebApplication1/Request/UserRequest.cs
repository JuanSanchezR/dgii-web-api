﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Request
{
    public class UserRequest
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string rncCedula { get; set; }
        public string tipo { get; set; }
        public string estatus { get; set; }
    }
}