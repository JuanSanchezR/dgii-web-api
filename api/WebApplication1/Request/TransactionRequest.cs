﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Request
{
    public class TransactionRequest
    {
        public int transactionid { get; set; }
        public string ncf { get; set; }
        public string usuarioid { get; set; }
        public string rncCedula { get; set; }
        public string monto { get; set; }
        public string itbis18 { get; set; }
        public DateTime fecha { get; set; }
    }
}