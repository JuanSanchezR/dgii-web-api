# dgii-web-api

## Correr la aplicacion

Esta es una aplicacion que se encuentra contenerizada para una mayor facilidad de uso y despliegue, debe contar con Docker Desktop instalado para poder ejecutarla, aqui el enlace de descarga https://www.docker.com/products/docker-desktop/.

1. Clona el repositorio localmente, `git clone https://gitlab.com/JuanSanchezR/dgii-web-api.git`
2. Luego de tener instalado Docker Desktop, proceder a abrirlo.
3. Ahora debes abrir tu terminal (PowerShell, cmd o cualquier otra de preferencia), situarnos en la carpeta "dgii-web-api" donde se encuentra el codigo y ejecutar el siguiente comando.
```sh
docker compose up -d --build
```

Nota: Si ocurre algun error y desea correrlo nuevamente ejecute el siguiente comando en la terminal: 

```sh
docker compose down -v && docker compose up -d --build --force-recreate
```

4. Cuando hayan iniciado todos los contenedores podras acceder al siguiente enlace: `http://localhost:3000/` para visualizar la app donde muestra los usuarios y al hacer click en un usuario podrá visualizar sus transacciones.

5. Si desea ver la documentacion de la API puede acceder al enlace `http://localhost:5000/swagger` donde mostrara la definición de cada endpoint y podra ejecutarlos y visualizar esos cambios cuando cuando usas el boton refrescar listado.

6. Para cerrar el contenedor debes abrir tu terminal (PowerShell, cmd o cualquier otra de preferencia) y ejecutar el siguiente comando.
```sh
docker compose down
```

----------------------------------------------------------------------------------------------------

Nota: Si presenta el error que se muestra aqui debajo en Windows es debido a bloqueos de firewall por el antivirus.
```
50.22 npm ERR! code ECONNRESET
50.22 npm ERR! network This is a problem related to network connectivity.
50.22 npm ERR! network In most cases you are behind a proxy or have bad network settings.
50.22 npm ERR! network
50.23 npm ERR! network If you are behind a proxy, please make sure that the
50.23 npm ERR! network 'proxy' config is set properly.  See: 'npm help config'
```