import './App.css';
import { useEffect, useState, Fragment } from 'react';

function Usuarios() {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [users, setUsers] = useState(null);

  const [expandedRows, setExpandedRows] = useState([]);

  const routes = {
    users: 'users',
    transactions: 'transactions',
  };

  const toggleRow = async (userId) => {
    const index = expandedRows.indexOf(userId);
    const newExpandedRows = [...expandedRows];

    if (index !== -1) {
      // The row is currently expanded, collapse it
      newExpandedRows.splice(index, 1);
    } else {
      // The row is currently collapsed, expand it
      newExpandedRows.push(userId);

      // Fetch the transaction records for the row
      const result = await fetchData(routes.transactions, userId);

      const transactions = !result.error ? result : [];

      // Store the transaction records in the user object
      setUsers((prevUsers) =>
        prevUsers.map((user) =>
          user.id === userId ? { ...user, transactions } : user
        )
      );
    }

    setExpandedRows(newExpandedRows);
  };

  const fetchData = async (route = routes.users, userId) => {
    try {
      const URL = 'http://localhost:5000/api';
      const response = await fetch(
        route === routes.transactions
          ? `${URL}/${route}/${userId}`
          : `${URL}/${route}`
      );
      if (!response.ok) console.error('Error, no está Ok');
      const jsonData = await response.json();
      return jsonData;
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await fetchData('users');
      setUsers(users);
    };

    fetchUsers();
  }, []);

  return (
    <div className="App">
      <button
        style={{
          fontSize: '16px',
          cursor: 'pointer',
          margin: '15px',
        }}
        onClick={() => window.location.reload()}
      >
        Refrescar listado
      </button>

      <>
        {isLoading && <div>Loading...</div>}
        {error && <div>Error: {error.message}</div>}

        <main>
          <div className="table-container">
            <div className="uk-overflow-auto">
              <table style={{ borderCollapse: 'collapse', width: '100%' }}>
                <thead>
                  <tr style={{ borderBottom: '1px solid #ddd' }}>
                    <th style={{ padding: '10px' }}>Nombre</th>
                    <th style={{ padding: '10px' }}>RNC/Cedula</th>
                    <th style={{ padding: '10px' }}>Tipo</th>
                    <th style={{ padding: '10px' }}>Estado</th>
                  </tr>
                </thead>
                <tbody>
                  {!isLoading &&
                    !error &&
                    users.map((user) => (
                      <Fragment key={user.id}>
                        <tr
                          onClick={() => toggleRow(user.id)}
                          style={{
                            cursor: 'pointer',
                            transition: 'background-color 0.3s ease',
                          }}
                          onMouseEnter={(e) =>
                            (e.currentTarget.style.backgroundColor = '#f0f0f0')
                          }
                          onMouseLeave={(e) =>
                            (e.currentTarget.style.backgroundColor = '')
                          }
                        >
                          <td>{user.nombre}</td>
                          <td>{user.rncCedula}</td>
                          <td>{user.tipo}</td>
                          <td>{user.estatus}</td>
                        </tr>
                        {expandedRows.includes(user.id) &&
                          user.transactions &&
                          !!user.transactions.length && (
                            <tr>
                              <td colSpan="4">
                                <table
                                  style={{
                                    borderCollapse: 'collapse',
                                    width: '100%',
                                    padding: '20px',
                                  }}
                                >
                                  <thead>
                                    <tr
                                      style={{ borderBottom: '1px solid #ddd' }}
                                    >
                                      <th
                                        style={{
                                          padding: '10px',
                                          fontSize: '12px',
                                          color: 'darkblue',
                                        }}
                                      >
                                        NCF
                                      </th>
                                      <th
                                        style={{
                                          padding: '10px',
                                          fontSize: '12px',
                                          color: 'darkblue',
                                        }}
                                      >
                                        rncCedula
                                      </th>
                                      <th
                                        style={{
                                          padding: '10px',
                                          fontSize: '12px',
                                          color: 'darkblue',
                                        }}
                                      >
                                        Monto
                                      </th>
                                      <th
                                        style={{
                                          padding: '10px',
                                          fontSize: '12px',
                                          color: 'darkblue',
                                        }}
                                      >
                                        Itbis
                                      </th>
                                      <th
                                        style={{
                                          padding: '10px',
                                          fontSize: '12px',
                                          color: 'darkblue',
                                        }}
                                      >
                                        Fecha
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {user.transactions?.map((transaction) => (
                                      <tr key={transaction.ncf}>
                                        <td>{transaction.ncf}</td>
                                        <td>{transaction.rncCedula}</td>
                                        <td>{transaction.monto}</td>
                                        <td>{transaction.itbis18}</td>
                                        <td>{transaction.fecha}</td>
                                      </tr>
                                    ))}
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          )}

                        {expandedRows.includes(user.id) &&
                          user.transactions &&
                          user.transactions.length === 0 && (
                            <tr>
                              <td colSpan="4">
                                No se encontraron datos para este usuario.
                              </td>
                            </tr>
                          )}
                      </Fragment>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </main>
      </>
    </div>
  );
}

export default Usuarios;
