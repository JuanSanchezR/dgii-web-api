import './App.css';

import Usuarios from './usuarios';

function App() {
  return (
    <div className="App">
      <Usuarios />
    </div>
  );
}

export default App;
